package net.zeiworld.kingdom_push.core;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DebugTest {

	KingdomPush game;
	
	@Before
	public void setUp(){
		game = new KingdomPush();
		game.StartMatch("Matthew", "Laura");
	}
	
	@After
	public void tearDown(){
		game = null;
	}
	
	@Test
	public void startGame() {
		// Ensure that the game has started properly.
		assertEquals(game.enabled, true);
		assertEquals(game.matchStarted, true);
	}
	
	@Test
	public void newUnit() {
		// Create a rogue and make sure it's been created.
		game.createUnit("rogue", 5);
		if (game.getTurn().getID() == 1){
			assertEquals(game.getUnit(0, 5).getType(), "rogue");
		}else{
			assertEquals(game.getUnit(game.getBattlefieldColumns()-1, 5).getType(), "rogue");
		}
	}
	
	@Test
	public void moveUnit() {
		// Move a mage around.
		game.createUnit("mage", 5);
		if (game.getTurn().getID() == 1){
			game.selectUnit("A", 5);
			game.moveUnit("B", 5);
			assertEquals(game.getUnit(1, 5).getType(), "mage");
		}else{
			game.selectUnit("T", 5);
			game.moveUnit("S", 5);
			assertEquals(game.getUnit(game.getBattlefieldColumns()-2, 5).getType(), "mage");
		}
	}
	
	@Test
	public void attackTest(){
		// Create a unit for the starting player.
		game.createUnit("knight", 1);
		game.endTurn();
		// Create a unit for the other player.
		game.createUnit("knight", 1);
		game.endTurn();
		// Make sure player 1 is the one moving their unit across the board.
		if (game.getTurn().getID() != 1){
			game.endTurn();
		}
		// Start moving the players knight across the board.
		game.selectUnit("A", 1);
		game.moveUnit("C", 1);
		game.moveUnit("E", 1);
		// Skip the other players turn.
		game.endTurn();
		game.endTurn();
		// Continue moving the knight across the board.
		game.moveUnit("G", 1);
		game.moveUnit("I", 1);
		game.moveUnit("K", 1);
		game.moveUnit("M", 1);
		game.moveUnit("O", 1);
		// Skip again.
		game.endTurn();
		game.endTurn();
		// Move the unit up to the enemy knight.
		game.moveUnit("Q", 1);
		game.moveUnit("S", 1);
		// Attack the enemy knight.
		game.attackUnit("T", 1);
		// Select the enemy knight.
		game.selectUnit("T", 1);
		// Check the selected knight's health.
		assertEquals(game.getSelected().getHealth(), new Integer(80));
		// Reselect the knight and attack and kill the enemy.
		game.selectUnit("S",1);
		game.attackUnit("T", 1);
		game.attackUnit("T", 1);
		game.endTurn();
		game.endTurn();
		game.attackUnit("T", 1);
		// The enemy knight is dead at this point, attacking the position again will directly cause damage to the player.
		game.attackUnit("T", 1);
		game.endTurn();
		// Check that the player took damage.
		assertEquals(game.getTurn().getHealth(), new Integer(85));
	}

}
