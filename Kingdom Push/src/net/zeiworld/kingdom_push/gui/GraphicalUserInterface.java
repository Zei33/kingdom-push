package net.zeiworld.kingdom_push.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GraphicalUserInterface extends Application {
	
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		
		try{
			
			//Prepare the javafx classes.
			FXMLLoader loader = new FXMLLoader();
			
			@SuppressWarnings("static-access")
			Parent root = loader.load(getClass().getResource("KingdomPushGUI.fxml"));
			Scene scene = new Scene(root);
			//GUIController control = (GUIController) loader.getController();
			
			// Load the stylesheet.
			scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			
			// Setup the window.
			primaryStage.setTitle("Kingdom Push");
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}catch(Exception e){
			
			e.printStackTrace();
			
		}
		
	}
	
}
