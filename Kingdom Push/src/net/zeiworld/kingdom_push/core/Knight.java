package net.zeiworld.kingdom_push.core;

import java.util.ArrayList;

public class Knight extends Unit {

	public Knight(int team, String type, Integer column, Integer row) {
		super(team, type, column, row);
		this.symbol = (team == 1 ? "♧" : "♣");
		this.hp = 110;
		this.attack = 30;
		this.moveCost = 20;
		this.attackCost = 20;
	}
	
	@Override
	public ArrayList<ArrayList<Integer>> getPossibleActions(String type){
		ArrayList<ArrayList<Integer>> actions = new ArrayList<ArrayList<Integer>>();
		
		if (type.equals("move")){
		
			// Knights can move straight and diagonally towards their enemies side.
			if (team == 1){
				actions.add( createAction(+1, -1) );
				actions.add( createAction(+1, 0) );
				actions.add( createAction(+2, 0) );
				actions.add( createAction(+1, +1) );
			}else{
				actions.add( createAction(-1, -1) );
				actions.add( createAction(-1, 0) );
				actions.add( createAction(-2, 0) );
				actions.add( createAction(-1, +1) );
			}
		
		}else if (type.equals("attack")){
			
			actions.add( createAction(-1, -1) );
			actions.add( createAction(0, -1) );
			actions.add( createAction(+1, -1) );
			actions.add( createAction(-1, 0) );
			actions.add( createAction(+1, 0) );
			actions.add( createAction(-1, +1) );
			actions.add( createAction(0, +1) );
			actions.add( createAction(+1, +1) );
		}
		
		return actions;
	}

}
