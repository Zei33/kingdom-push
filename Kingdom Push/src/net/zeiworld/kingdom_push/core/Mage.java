package net.zeiworld.kingdom_push.core;

import java.util.ArrayList;

public class Mage extends Unit {

	public Mage(int team, String type, Integer column, Integer row) {
		super(team, type, column, row);
		this.symbol = (team == 1 ? "♤" : "♠");
		this.hp = 40;
		this.attack = 90;
		this.moveCost = 15;
		this.attackCost = 30;
	}

	@Override
	public ArrayList<ArrayList<Integer>> getPossibleActions(String type){
		ArrayList<ArrayList<Integer>> actions = new ArrayList<ArrayList<Integer>>();
		
		if (type.equals("move")){
		
			// Mages can only move straight forward towards their enemies side.
			if (team == 1){
				actions.add( createAction(+1, 0) );
			}else{
				actions.add( createAction(-1, 0) );
			}
		
		}else if (type.equals("attack")){
			
			actions.add( createAction(0, -3) );
			actions.add( createAction(-2, -2) );
			actions.add( createAction(0, -2) );
			actions.add( createAction(+2, -2) );
			actions.add( createAction(-1, -1) );
			actions.add( createAction(0, -1) );
			actions.add( createAction(+1, -1) );
			actions.add( createAction(-3, 0) );
			actions.add( createAction(-2, 0) );
			actions.add( createAction(-1, 0) );
			actions.add( createAction(+1, 0) );
			actions.add( createAction(+2, 0) );
			actions.add( createAction(+3, 0) );
			actions.add( createAction(-1, +1) );
			actions.add( createAction(0, +1) );
			actions.add( createAction(+1, +1) );
			actions.add( createAction(-2, +2) );
			actions.add( createAction(0, +2) );
			actions.add( createAction(+2, +2) );
			actions.add( createAction(0, +3) );
			
		}
		
		return actions;
	}
	
}
