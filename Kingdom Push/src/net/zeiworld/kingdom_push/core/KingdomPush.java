package net.zeiworld.kingdom_push.core;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import static java.nio.file.StandardOpenOption.*;

public class KingdomPush {

	public boolean enabled = true;
	public boolean matchStarted = false;
	public ArrayList<Integer> trackSelected;
	
	private ArrayList<Player> players;
	private ArrayList<ArrayList<Unit>> battlefield;
	private int turn;
	
	private String[] columnLabels = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	
	public void StartMatch(String PlayerName1, String PlayerName2){
		
		// Instantiate the player list.
		players = new ArrayList<Player>();
		
		// Generate the battlefield grid.
		battlefield = generateBattlefield(20,10);
		
		// Create Players.
		players.add(new Player(PlayerName1, 1));
		players.add(new Player(PlayerName2, 2));
		
		//Instantiate unit selection.
		trackSelected = new ArrayList<Integer>();
		trackSelected.add(null);
		trackSelected.add(null);
		
		// Pick a random player to have the first turn and begin the match.
		turn = (int) Math.round(Math.random());
		matchStarted = true;
		
		// Announce Match Start!
		System.out.println("\nLet the epic battle between the kingdoms of " + getPlayer(1).getTitle(true) + getPlayer(1).getName() + " and " + getPlayer(2).getTitle(true) + getPlayer(2).getName() + " begin!");
		
		drawBattlefield(false);
		showStats();
		
	}
	
	public Player getPlayer(Integer ID){
		// ID will be the actual playerID (so 1 or 2) but in the list it will be 0 or 1.
		return players.get(ID-1);
	}
	
	public Player getTurn(){
		return players.get(turn);
	}
	
	private ArrayList<ArrayList<Unit>> generateBattlefield(Integer columns, Integer rows){
		ArrayList<ArrayList<Unit>> newBattlefield = new ArrayList<ArrayList<Unit>>();
		
		for(int c = 0; c < columns; c++){
			
			newBattlefield.add(new ArrayList<Unit>());
			
			for(int r = 0; r < rows; r++){
				
				newBattlefield.get(c).add(null);
				
			}
			
		}
		
		return newBattlefield;
	}
	
	public void drawBattlefield(boolean showAttack){
		
		System.out.println();
		
		boolean checkSelected = false;
		
		if (trackSelected.get(0) != null){
			checkSelected = true;
		}
		
		for(int r = 0; r < battlefield.get(0).size(); r++){
			
			if (r == 0){
				//Print Column Labels.
				System.out.print("  ");
				for(int i = 0; i < battlefield.size(); i++){
					System.out.print(columnLabels[i] + " ");
				}
			}
			
			for(int c = 0; c < battlefield.size(); c++){
				if (c == 0){
					System.out.print("\n" + r + " ");
				}
				
				if (battlefield.get(c).get(r) == null){
					
					if (checkSelected == false){
					System.out.print("□");
					}else if (showAttack == false){
						ArrayList<ArrayList<Integer>> CheckForMovement = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("move");
						boolean moveHere = false;
						for(int i = 0;i<CheckForMovement.size();i++){
							if (c == CheckForMovement.get(i).get(0) && r == CheckForMovement.get(i).get(1)){
								System.out.print("■");
								moveHere = true;
							}
						}
						if (moveHere == false){
							System.out.print("□");
						}
					}else if (showAttack == true){
						ArrayList<ArrayList<Integer>> CheckForAttack = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("attack");
						boolean attackHere = false;
						for(int i = 0;i<CheckForAttack.size();i++){
							if (c == CheckForAttack.get(i).get(0) && r == CheckForAttack.get(i).get(1)){
								System.out.print("■");
								attackHere = true;
							}
						}
						if (attackHere == false){
							System.out.print("□");
						}
					}
					
					if ( c < battlefield.size()-1){
						if (battlefield.get(c+1).get(r) != null){
							
							if (battlefield.get(c+1).get(r).getSelected()){
								System.out.print(">");
							}else if (showAttack == true){
								ArrayList<ArrayList<Integer>> CheckForAttack = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("attack");
								boolean attackHere = false;
								for(int i = 0;i<CheckForAttack.size();i++){
									if (c+1 == CheckForAttack.get(i).get(0) && r == CheckForAttack.get(i).get(1)){
										System.out.print("!");
										attackHere = true;
									}
								}
								
								if (attackHere == false){
									System.out.print("-");
								}
							}else{
								System.out.print("-");
							}
							
						}else{
							System.out.print(" ");
						}
					}else{
						System.out.print(" ");
					}
				}else{
					if (battlefield.get(c).get(r).getSelected()){
						System.out.print( battlefield.get(c).get(r).getSymbol() + "<" );
					}else if (showAttack == true){
						ArrayList<ArrayList<Integer>> CheckForAttack = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("attack");
						boolean attackHere = false;
						for(int i = 0;i<CheckForAttack.size();i++){
							if (c == CheckForAttack.get(i).get(0) && r == CheckForAttack.get(i).get(1)){
								System.out.print( battlefield.get(c).get(r).getSymbol() + "!" );
								attackHere = true;
							}
						}
						
						if (attackHere == false){
							System.out.print( battlefield.get(c).get(r).getSymbol() + "-" );
						}
					}else{
						System.out.print( battlefield.get(c).get(r).getSymbol() + "-" );
					}
				}
				
				if (c == battlefield.size()-1){
					System.out.print(r);
				}
			}
			
			if (r == battlefield.get(0).size()-1){
				//Print Column Labels.
				System.out.print("\n  ");
				for(int i = 0; i < battlefield.size(); i++){
					System.out.print(columnLabels[i] + " ");
				}
			}
			
		}
		
		System.out.println();
		
	}
	
	public Unit getUnit(int column, int row){
		return battlefield.get(column).get(row);
	}
	
	public Integer getBattlefieldColumns(){
		return battlefield.size();
	}
	
	public Integer getBattlefieldRows(){
		return battlefield.get(0).size();
	}
	
	public void showStats(){
		System.out.println("Health: " + getTurn().getHealth() + " | Energy: " + getTurn().getEnergy() + "/" + getTurn().getMaxEnergy());
	}
	
	public boolean createUnit(String type, Integer row){
		if (getTurn().getEnergy() >= 50){
			Integer column = (getTurn().getID() == 1 ? 0 : battlefield.size()-1);
			if (battlefield.get(column).get(row) == null){
				Unit newUnit = null;
				if (type.equals("knight")){
					newUnit = new Knight(getTurn().getID(), type, column, row);
				} else if (type.equals("mage")) {
					newUnit = new Mage(getTurn().getID(), type, column, row);
				} else if (type.equals("rogue")) {
					newUnit = new Rogue(getTurn().getID(), type, column, row);
				}
				battlefield.get(column).set(row, newUnit);
				getTurn().setEnergy(-50);
				return true;
			}else{
				System.out.println("There is already a unit at that position.");
				return false;
			}
		}else{
			System.out.println("You need at least 50 energy to bring the unit on to the battlefield.");
			return false;
		}
	}
	
	public boolean selectUnit(String columnLetter, Integer row){
		
		Integer column = getColumnNumber(columnLetter);
		
		if (column == -1){
			return false;
		}
		
		if (battlefield.get(column).get(row) != null){
			// Tell the last selected unit that it is no longer selected.
			if (getSelected() != null){
				getSelected().setSelected(false);
			}
			// Select the new unit.
			getUnit(column, row).setSelected(true);
			// The track selected array tracks the position of the unit that is currently selected.
			trackSelected(column, row);
			
			drawBattlefield(false);
			showSelectedStats();
			return true;
		}else{
			return false;
		}
	}
	
	public void selectUnitGUI(Integer column, Integer row){
		
		// A much simpler version of the selectUnit() function designed to work with the GUI which doesn't require column letters.
		
		if (battlefield.get(column).get(row) != null){
			
			if (getSelected() != null){
				getSelected().setSelected(false);
			}
			
			getUnit(column,row).setSelected(true);
			trackSelected(column, row);
			
		}
		
	}
	
	public void trackSelected(Integer column, Integer row){
		
		trackSelected.set(0, column);
		trackSelected.set(1, row);
		
	}
	
	public Unit getSelected(){
		
		if (trackSelected.get(0) != null){
			return battlefield.get(trackSelected.get(0)).get(trackSelected.get(1));
		}else{
			return null;
		}
		
	}
	
	public void showSelectedStats(){
		
		System.out.println(getPlayer(getSelected().getTeam()).getName() + "'s " + getSelected().getType().substring(0, 1).toUpperCase() + getSelected().getType().substring(1) 
				+ " | Health: " + getSelected().getHealth() + " | Attack: " + getSelected().getAttack());
		
	}
	
	public Integer getColumnNumber(String column){
		
		Integer columnNumber = -1;
		
		for(int i = 0;i < columnLabels.length-1;i++){
			if (columnLabels[i].equals(column.toUpperCase())){
				columnNumber = i;
			}
		}
		
		return columnNumber;
		
	}
	
	public boolean moveUnit(String columnLetter, Integer row){
		
		if (getTurn().getID() != getSelected().getTeam()-1){
		
			if (getTurn().getEnergy() >= getSelected().getMoveCost()){
			
				Integer column = getColumnNumber(columnLetter);
				
				if (column == -1){
					return false;
				}
				
				if (battlefield.get(column).get(row) == null){
					
					ArrayList<ArrayList<Integer>> CheckForMovement = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("move");
					boolean moveHere = false;
					for(int i = 0;i<CheckForMovement.size();i++){
						if (column == CheckForMovement.get(i).get(0) && row == CheckForMovement.get(i).get(1)){
							moveHere = true;
						}
					}
					
					if (moveHere == false){
						System.out.println("The unit can't move to this position.");
						return false;
					}
					
					// Put the unit in the new position.
					battlefield.get(column).set(row, battlefield.get( getSelected().getColumn() ).get( getSelected().getRow() ));
					
					// Clear the old position of the unit.
					battlefield.get( getSelected().getColumn() ).set( getSelected().getRow(), null );
					
					// Track the unit's coordinates on the map.
					trackSelected(column, row);
					
					// Set the unit's coordinates.
					getSelected().setPosition(column, row);
					
					getTurn().setEnergy(-getSelected().getMoveCost());
					
					drawBattlefield(false);
					showStats();
					showSelectedStats();
					
					return true;
					
				}else{
					System.out.println("There is already a unit in this position.");
					return false;
				}
			
			}else{
				System.out.println("This unit requires atleast " + getSelected().getMoveCost().toString() + " energy to move.");
				return false;
			}
		
		}else{
			System.out.println("You can only move your own units.");
			return false;
		}
		
	}
	
	public void moveUnitGUI(Integer column, Integer row){
		
		// A simplified version of moveUnit() without position checking since the GUI will only ever show the correct buttons.
		
		// Put the unit in the new position.
		battlefield.get(column).set(row, battlefield.get( getSelected().getColumn() ).get( getSelected().getRow() ));
		
		// Clear the old position of the unit.
		battlefield.get( getSelected().getColumn() ).set( getSelected().getRow(), null );
		
		// Track the unit's coordinates on the map.
		trackSelected(column, row);
		
		// Set the unit's coordinates.
		getSelected().setPosition(column, row);
		
		getTurn().setEnergy(-getSelected().getMoveCost());
		
	}
	
	public boolean attackUnit(String columnLetter, Integer row){
		
		if (getTurn().getID() != getSelected().getTeam()-1){
			
			if (getTurn().getEnergy() >= getSelected().getAttackCost()){
				
				Integer column = getColumnNumber(columnLetter);
				
				if (column == -1){
					return false;
				}
				
				if (battlefield.get(column).get(row) != null){
					
					ArrayList<ArrayList<Integer>> CheckForAttack = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("attack");
					boolean attackHere = false;
					for(int i = 0;i<CheckForAttack.size();i++){
						if (column == CheckForAttack.get(i).get(0) && row == CheckForAttack.get(i).get(1)){
							attackHere = true;
						}
					}
					
					if (attackHere == false){
						System.out.println("That position is out of range.");
						return false;
					}
					
					System.out.println("Your " + getSelected().getType() + " deals " + getSelected().getAttack() + " damage to the enemy " + getUnit(column,row).getType() + ".");
					getUnit(column, row).setHealth(-getSelected().getAttack());
					
					if (getUnit(column, row).getHealth() <= 0){
						System.out.println("Your " + getSelected().getType() + " has killed the enemy " + getUnit(column,row).getType() + ".");
						battlefield.get(column).set(row, null);
					}
					
					getTurn().setEnergy(-getSelected().getAttackCost());
					
					return true;
					
				}else if ((column == 0 && getTurn().getID() == 2) || (column == battlefield.size()-1  && getTurn().getID() == 1)){
					
					ArrayList<ArrayList<Integer>> CheckForAttack = getUnit(trackSelected.get(0), trackSelected.get(1)).getPossibleActions("attack");
					boolean attackHere = false;
					for(int i = 0;i<CheckForAttack.size();i++){
						if (column == CheckForAttack.get(i).get(0) && row == CheckForAttack.get(i).get(1)){
							attackHere = true;
						}
					}
					
					if (attackHere == false){
						System.out.println("That position is out of range.");
						return false;
					}
					
					Integer enemy = null;
					if (turn == 0){
						enemy = 2;
					}else if (turn == 1){
						enemy = 1;
					}
					
					System.out.println("Your " + getSelected().getType() + " attacks " + getPlayer(enemy).getTitle(true) 
							+ getPlayer(enemy).getName() + " directly and deals " + getSelected().getAttack()/2 + " damage.");
					
					getPlayer(enemy).setHealth(-(getSelected().getAttack()/2));
					getTurn().setEnergy(-getSelected().getAttackCost());
					
					if (getPlayer(enemy).getHealth() <= 0){
						System.out.println("\n\n" + getPlayer(enemy).getTitle(true) + getPlayer(enemy).getName() + " has been defeated!");
						System.out.println("The winner of this battle is " + getTurn().getTitle(true) + getTurn().getName() + "!");
						
						enabled = false;
					}
					
					return true;
					
				}else{
					System.out.println("There is no unit at that position.");
					return false;
				}
				
			}else{
				System.out.println("This unit requires atleast " + getSelected().getAttackCost().toString() + " energy to attack.");
				return false;
			}
			
		}else{
			System.out.println("You can only command your own units.");
			return false;
		}
			
	}
	
	public void attackUnitGUI(Integer column, Integer row){
		
		getUnit(column, row).setHealth(-getSelected().getAttack());
		
		if (getUnit(column, row).getHealth() <= 0){
			battlefield.get(column).set(row, null);
		}
		
		getTurn().setEnergy(-getSelected().getAttackCost());
		
	}
	
	public boolean attackDirectGUI(){
		
		Integer enemy = null;
		if (turn == 0){
			enemy = 2;
		}else if (turn == 1){
			enemy = 1;
		}
		
		getPlayer(enemy).setHealth(-(getSelected().getAttack()/2));
		getTurn().setEnergy(-getSelected().getAttackCost());
		
		if (getPlayer(enemy).getHealth() <= 0){
			
			enabled = false;
			return true;
			
		}
		
		return false;
		
	}
	
	public void endTurn(){
		
		turn = (turn == 0 ? 1 : 0);
		
		getTurn().setMaxEnergy(+10);
		
		// Add energy equal to the players health up to the maximum energy value.
		if ( ( getTurn().getEnergy() + getTurn().getHealth() ) > getTurn().getMaxEnergy() ){
			getTurn().hardSetEnergy(getTurn().getMaxEnergy());
		}else{
			getTurn().setEnergy(getTurn().getHealth());
		}
		
		drawBattlefield(false);
		showStats();
		
	}
	
	public String readHighscore(){
		
		Path file = Paths.get("./highscores.txt");
		try (InputStream in = Files.newInputStream(file);
		    BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		        return line;
		    }
		} catch (IOException x) {
		    System.err.println(x);
		}
		return null;
		
	}
	
	public void writeHighscore(String write){
	    byte data[] = write.getBytes();
	    Path path = Paths.get("./highscores.txt");

	    try (OutputStream out = new BufferedOutputStream(
	      Files.newOutputStream(path, CREATE, TRUNCATE_EXISTING))) {
	      out.write(data, 0, data.length);
	    } catch (IOException x) {
	      System.err.println(x);
	    }
		
	}
	
}
