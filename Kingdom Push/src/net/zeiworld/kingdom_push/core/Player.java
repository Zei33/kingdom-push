package net.zeiworld.kingdom_push.core;

import java.util.Random;

public class Player {

	private String name;
	private Integer id;
	
	private String title;
	private Integer hp;
	private Integer energy;
	private Integer maxEnergy;
	
	private final String[] titleList = { "Lord", "Master", "King", "Baron", "Duke", "Prince", "Queen", "Princess", "Count" };
	
	public Player(String name, Integer id){
		
		this.name = name;
		this.id = id;
		
		this.title = titleList[ new Random().nextInt(titleList.length) ];
		this.hp = 100;
		this.energy = 100;
		this.maxEnergy = 100;
		
	}
	
	public String getName(){
		return name;
	}
	
	public Integer getID(){
		return id;
	}
	
	public String getTitle(boolean appendSpace){
		return title + ( appendSpace ? " " : "");
	}
	
	public Integer getHealth(){
		return hp;
	}
	
	public Integer getEnergy(){
		return energy;
	}
	
	public Integer getMaxEnergy(){
		return maxEnergy;
	}
	
	public void setHealth(Integer value){
		hp += value;
	}
	
	public void setEnergy(Integer value){
		energy += value;
	}
	
	public void hardSetEnergy(Integer value){
		energy = value;
	}
	
	public void setMaxEnergy(Integer value){
		maxEnergy += value;
	}
	
	@Override
	public String toString(){
		return "[PlayerName=" + name + ", PlayerID= " + id + "]";
	}
	
}
