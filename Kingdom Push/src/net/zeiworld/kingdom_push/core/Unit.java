package net.zeiworld.kingdom_push.core;

import java.util.ArrayList;

public class Unit {

	private String type;
	private Integer column;
	private Integer row;
	private boolean selected;
	
	protected String symbol;
	protected Integer hp;
	protected Integer attack;
	protected Integer moveCost;
	protected Integer attackCost;
	protected int team;
	
	public Unit(int team, String type, Integer column, Integer row){
		this.team = team;
		this.type = type;
		this.column = column;
		this.row = row;
		this.selected = false;
	}
	
	public int getTeam(){
		return team;
	}
	
	public String getType(){
		return type;
	}
	
	public Integer getColumn(){
		return column;
	}
	
	public Integer getRow(){
		return row;
	}
	
	public String getSymbol(){
		return symbol;
	}
	
	public Integer getHealth(){
		return hp;
	}
	
	public Integer getMoveCost(){
		return moveCost;
	}
	
	public Integer getAttackCost(){
		return moveCost;
	}
	
	public Integer getAttack(){
		return attack;
	}
	
	public boolean getSelected(){
		return selected;
	}
	
	public void setHealth(Integer value){
		// Can be a negative value.
		hp += value;
	}
	
	public void setSelected(boolean state){
		selected = state;
	}
	
	public void setPosition(Integer newColumn, Integer newRow){
		column = newColumn;
		row = newRow;
	}
	
	public ArrayList<ArrayList<Integer>> getPossibleActions(String type){
		
		ArrayList<ArrayList<Integer>> actions = new ArrayList<ArrayList<Integer>>();
		
		if (type.equals("move")){
		
		}else if (type.equals("attack")){
			
		}
		
		return actions;
		
	}
	
	protected ArrayList<Integer> createAction(Integer relativeColumn, Integer relativeRow){
		ArrayList<Integer> position = new ArrayList<Integer>();
		position.add(this.column + relativeColumn);
		position.add(this.row + relativeRow);
		return position;
	}
	
	@Override
	public String toString(){
		return "Unit";
	}
	
}
