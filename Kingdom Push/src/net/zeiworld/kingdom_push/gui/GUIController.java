package net.zeiworld.kingdom_push.gui;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import net.zeiworld.kingdom_push.core.KingdomPush;
import net.zeiworld.kingdom_push.core.Player;
import net.zeiworld.kingdom_push.core.Unit;

public class GUIController {
	
	private KingdomPush game;
	private String mode;
	private String spawnType;
	
	@FXML
	private TextField inputName1;
	@FXML
	private TextField inputName2;
	@FXML
	private Text player1Name;
	@FXML
	private Text player2Name;
	@FXML
	private Text player1Health;
	@FXML
	private Text player2Health;
	@FXML
	private Text player1Energy;
	@FXML
	private Text player2Energy;
	@FXML
	private Text selectedName;
	@FXML
	private Text selectedHealth;
	@FXML
	private Text selectedAttack;
	@FXML
	private Text selectedMoveCost;
	@FXML
	private Text selectedAttackCost;
	@FXML
	private Text highscore;
	@FXML
	private Button spawnKnight;
	@FXML
	private Button spawnRogue;
	@FXML
	private Button spawnMage;
	@FXML
	private Button moveSelected;
	@FXML
	private Button attackSelected;
	@FXML
	private Button endTurn;
	@FXML
	private GridPane battlefield;
	@FXML
	private Label instructionsTitle;
	@FXML
	private Label instructionsDescription;
	@FXML
	private Text winnerDescription;
	@FXML
	private Text winnerName;
	
	public void startGame(ActionEvent event){
		
		if (inputName1.getText().length() >= 1 && inputName1.getText().length() < 15 && inputName2.getText().length() >= 1 && inputName2.getText().length() < 15){
			game = new KingdomPush();
			game.StartMatch(inputName1.getText(), inputName2.getText());
			
			inputName1.setVisible(false);
			inputName2.setVisible(false);
			instructionsTitle.setVisible(false);
			instructionsDescription.setVisible(false);
			
			Button target = (Button) event.getSource();
			target.setVisible(false);
			
			player1Name.setText(game.getPlayer(1).getTitle(true) + game.getPlayer(1).getName().substring(0, 1).toUpperCase() + game.getPlayer(1).getName().substring(1));
			player2Name.setText(game.getPlayer(2).getTitle(true) + game.getPlayer(2).getName().substring(0, 1).toUpperCase() + game.getPlayer(2).getName().substring(1));
			
			spawnKnight.setDisable(false);
			spawnRogue.setDisable(false);
			spawnMage.setDisable(false);
			endTurn.setDisable(false);
			
			updatePlayerStats();
		}
		
	}
	
	public void updatePlayerStats(){
		
		// Update Health and Energy.
		player1Health.setText("Health: " + game.getPlayer(1).getHealth() + " / 100");
		player2Health.setText("Health: " + game.getPlayer(2).getHealth() + " / 100");
		player1Energy.setText("Energy: " + game.getPlayer(1).getEnergy() + " / " + game.getPlayer(1).getMaxEnergy());
		player2Energy.setText("Energy: " + game.getPlayer(2).getEnergy() + " / " + game.getPlayer(2).getMaxEnergy());
		
		// Highlight the name of the player who's turn it is.
		if ( game.getTurn().getID().equals(1) ){
			player1Name.setFill(Color.YELLOW);
			player2Name.setFill(Color.BLACK);
		}else{
			player1Name.setFill(Color.BLACK);
			player2Name.setFill(Color.YELLOW);
		}
		
	}
	
	public void spawnUnit(ActionEvent event){
		
		mode = "spawning";
		
		Button target = (Button) event.getSource();
		spawnType = target.getText();
		
		spawnKnight.setDisable(true);
		spawnRogue.setDisable(true);
		spawnMage.setDisable(true);
		moveSelected.setDisable(true);
		attackSelected.setDisable(true);
		endTurn.setDisable(true);
		
		showSpawnLocations();
		
	}
	
	public void showSpawnLocations(){
		
		updateBattlefield();
		
		for(int r = 0; r < game.getBattlefieldRows(); r++){
			
			Button spawnButton = new Button();
			spawnButton.setId("spawnButton|" + r);
			spawnButton.setOnAction(new EventHandler<ActionEvent>(){
				public void handle(ActionEvent event) {
	                 spawnUnit((Button) event.getSource());
	            }
			});
			spawnButton.setPrefSize(55, 55);
			spawnButton.setStyle("-fx-background-color:rgba(33,150,243,0.5)");
			
			if (game.getTurn().getID().equals(1)){
				
				if (getNodeFromGridPane(battlefield, 0, r) == null){
					battlefield.add(spawnButton, 0, r);
				}
				
			}else{
				
				if (getNodeFromGridPane(battlefield, game.getBattlefieldColumns()-1, r) == null){
					battlefield.add(spawnButton, game.getBattlefieldColumns()-1, r);
				}
				
			}
			
		}
		
	}
	
	public void cancelAction(MouseEvent event){
		
		if (event.getButton() == MouseButton.SECONDARY){
			
			// Cancel spawning.
			if (mode == "spawning"){
				mode = "";
				spawnType = "";
				removeSpawnButtons();
				updateButtonState();
			}
			
			// Cancel attacking.
			if (mode == "attacking"){
				mode = "";
				updateBattlefield();
				updateButtonState();
			}
			
			// Cancel moving.
			if (mode == "moving"){
				mode = "";
				updateBattlefield();
				updateButtonState();
			}
			
		}
		
	}
	
	private void removeSpawnButtons(){
		for(int c = 0; c < game.getBattlefieldColumns();c++){
			for(int r = 0; r < game.getBattlefieldRows();r++){
				
				// Get specific node from the battlefield grid and remove any spawn buttons.
				Node child = getNodeFromGridPane(battlefield, c, r);
				if (child != null){
					if (child.getId().equals("spawnButton|" + r)){
						battlefield.getChildren().remove(child);
					}
				}
				
			}
		}
	}
	
	private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
	    
		for (Node node : gridPane.getChildren()) {
	    	
	        if (node instanceof Button && GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
	            return node;
	        }
	        
	    }
	    return null;
	    
	}
	
	public void updateButtonState(){
		
		Player currentPlayer = game.getTurn();
		Unit selectedUnit = game.getSelected();
		
		if (currentPlayer.getEnergy() >= 50 && mode != "attacking" && mode != "moving"){
			spawnKnight.setDisable(false);
			spawnRogue.setDisable(false);
			spawnMage.setDisable(false);
		}else{
			spawnKnight.setDisable(true);
			spawnRogue.setDisable(true);
			spawnMage.setDisable(true);
		}
		
		if (selectedUnit != null){
			if (selectedUnit.getTeam() == currentPlayer.getID()){
				
				if (selectedUnit.getAttackCost() <= currentPlayer.getEnergy() && mode != "spawning" && mode != "moving"){
					attackSelected.setDisable(false);
				}else{
					attackSelected.setDisable(true);
				}
				
				if (selectedUnit.getMoveCost() <= currentPlayer.getEnergy() && mode != "spawning" && mode != "attacking"){
					moveSelected.setDisable(false);
				}else{
					moveSelected.setDisable(true);
				}
				
			}else{
				
				attackSelected.setDisable(true);
				moveSelected.setDisable(true);
				
			}
		}
		
		if (mode == ""){
			endTurn.setDisable(false);
		}else{
			endTurn.setDisable(true);
		}
		
	}
	
	public void spawnUnit(Button target){
		
		String rowString = target.getId().substring(target.getId().indexOf("|")+1);
		Integer row = Integer.parseInt(rowString);
		game.createUnit(spawnType.toLowerCase(), row);
		mode = "";
		spawnType = "";
		removeSpawnButtons();
		updateButtonState();
		updatePlayerStats();
		updateBattlefield();
		
	}
	
	public void updateBattlefield(){
		
		for(int r = 0; r < game.getBattlefieldRows(); r++){
			for(int c = 0; c < game.getBattlefieldColumns(); c++){
				Unit unit = game.getUnit(c, r);
				if (unit != null){
					
					// Clear the cell before getting started
					battlefield.getChildren().remove(getNodeFromGridPane(battlefield, c, r));
					
					Button unitButton = new Button();
					unitButton.setId("unitButton|" + c + "-" + r);
					unitButton.setOnAction(new EventHandler<ActionEvent>(){
						public void handle(ActionEvent event) {
			                 selectUnit(event);
			            }
					});
					unitButton.setPrefSize(55, 55);
					
					String sprite = "";
					sprite += unit.getType().toLowerCase();
					if (unit.getTeam() == 1){
						sprite += "+";
						
					}
					String image = getClass().getResource(sprite + ".png").toExternalForm();
					
					boolean attackHere = false;
					boolean allyHere = false;
					if (mode != "moving" && game.getSelected() != null){
						ArrayList<ArrayList<Integer>> CheckForAttack = game.getSelected().getPossibleActions("attack");
						for(int i = 0; i < CheckForAttack.size(); i++){
							if (c == CheckForAttack.get(i).get(0) && r == CheckForAttack.get(i).get(1)){
								if (game.getUnit(c,r).getTeam() != game.getSelected().getTeam()){
									attackHere = true;
								}else{
									allyHere = true;
								}
							}
						}
					}
					
					unitButton.setStyle("-fx-background-image:url(" + image + ");"
							+ "-fx-background-repeat:no-repeat;"
							+ "-fx-background-color:transparent;"
							+ "-fx-background-position:center center;" 
							+ (unit == game.getSelected() ? "-fx-background-color:rgba(255,235,59,0.8);" : "")
							+ (attackHere ? "-fx-background-color:rgba(244,67,54,0.8);" : "")
							+ (allyHere ? "-fx-background-color:rgba(1,87,155,0.8);" : ""));
					
					battlefield.add(unitButton, c, r);
					
				}else if (unit == null){
					// Clear cells that previously held units.
					battlefield.getChildren().remove(getNodeFromGridPane(battlefield, c, r));
					
					if (game.getSelected() != null && mode != "spawning"){
						
						ArrayList<ArrayList<Integer>> CheckForMovement = game.getUnit(game.trackSelected.get(0), game.trackSelected.get(1)).getPossibleActions("move");
						boolean moveHere = false;
						ArrayList<ArrayList<Integer>> CheckForAttack = game.getUnit(game.trackSelected.get(0), game.trackSelected.get(1)).getPossibleActions("attack");
						boolean attackHere = false;
						boolean directHere = false;
						
						if (mode != "attacking"){
							for(int i = 0; i < CheckForMovement.size(); i++){
								if (c == CheckForMovement.get(i).get(0) && r == CheckForMovement.get(i).get(1)){
									moveHere = true;
								}
							}
						}
						
						if (mode != "moving"){
							for(int i = 0; i < CheckForAttack.size(); i++){
								if (c == CheckForAttack.get(i).get(0) && r == CheckForAttack.get(i).get(1)){
									if ((c == 0 && game.getTurn().getID() == 2) || (c == game.getBattlefieldColumns()-1 && game.getTurn().getID() == 1)){
										directHere = true;
									}else{
										attackHere = true;
									}
									attackHere = true;
								}
							}
						}
						
						Button possibleButton = new Button();
						possibleButton.setId("possibleButton|" + c + "-" + r);
						possibleButton.setPrefSize(55, 55);
						possibleButton.setOnAction(new EventHandler<ActionEvent>(){
							public void handle(ActionEvent event) {
				                 possibleAction(event);
				            }
						});
						
						Button directButton = new Button();
						directButton.setId("directButton");
						directButton.setPrefSize(55, 55);
						directButton.setOnAction(new EventHandler<ActionEvent>(){
							public void handle(ActionEvent event){
								directAction(event);
							}
						});
						
						if (directHere){
							//Create deep red cell.
							directButton.setStyle("-fx-background-color:rgba(250,67,54,0.9);");
							battlefield.add(directButton, c, r);
						}else if (moveHere && attackHere){
							// Create purple cell.
							possibleButton.setStyle("-fx-background-color:rgba(103,58,183,0.6);");
							battlefield.add(possibleButton, c, r);
						}else if (moveHere){
							// Create blue cell.
							possibleButton.setStyle("-fx-background-color:rgba(33,150,243,0.6);");
							battlefield.add(possibleButton, c, r);
						}else if (attackHere){
							// Create red cell.
							possibleButton.setStyle("-fx-background-color:rgba(244,67,54,0.6);");
							battlefield.add(possibleButton, c, r);
						}
						
					}
					
				}
			}
		}
		
	}
	
	public void selectUnit(ActionEvent event){
		
		Button unit = (Button) event.getSource();
		int column = Integer.parseInt(unit.getId().substring(unit.getId().indexOf("|")+1, unit.getId().indexOf("-")));
		int row = Integer.parseInt(unit.getId().substring(unit.getId().indexOf("-")+1));
		
		if (mode != "attacking"){
			
			game.selectUnitGUI(column, row);
			moveSelected.setDisable(false);
			attackSelected.setDisable(false);
			
			updateBattlefield();
			updateSelectedStats();
			updateButtonState();
			
		}else{
			
			if (game.getSelected() != null){
				ArrayList<ArrayList<Integer>> CheckForAttack = game.getSelected().getPossibleActions("attack");
				for(int i = 0; i < CheckForAttack.size(); i++){
					if (column == CheckForAttack.get(i).get(0) && row == CheckForAttack.get(i).get(1)){
						if (game.getUnit(column,row).getTeam() != game.getSelected().getTeam()){
							game.attackUnitGUI(column,row);
							if (game.getSelected().getAttackCost() > game.getTurn().getEnergy()){ 
								mode = "";
							}
							updateBattlefield();
							updateButtonState();
							updatePlayerStats();
						}
					}
				}
			}
			
		}
		
	}
	
	public void updateSelectedStats(){
		
		String playerName = game.getPlayer(game.getSelected().getTeam()).getName();
		String unitType = game.getSelected().getType();
		selectedName.setText( playerName.substring(0, 1).toUpperCase() + playerName.substring(1)  + "'s " + unitType.substring(0, 1).toUpperCase() + unitType.substring(1));
		selectedHealth.setText( "Health: " + game.getSelected().getHealth().toString() );
		selectedMoveCost.setText( "Move Cost: " + game.getSelected().getMoveCost().toString() );
		selectedAttack.setText( "Attack Damage: " + game.getSelected().getAttack().toString() );
		selectedAttackCost.setText( "Attack Cost: " + game.getSelected().getMoveCost().toString());
		
	}
	
	public void attackUnit(){
		mode = "attacking";
		updateBattlefield();
		updateButtonState();
	}
	
	public void moveUnit(){
		mode = "moving";
		updateBattlefield();
		updateButtonState();
	}
	
	public void possibleAction(ActionEvent event){
		
		Button target = (Button) event.getSource();
		
		if (mode == "moving"){
			game.moveUnitGUI( Integer.parseInt( target.getId().substring( target.getId().indexOf("|")+1, target.getId().indexOf("-") ) ), Integer.parseInt( target.getId().substring( target.getId().indexOf("-")+1 ) ) );
			if (game.getSelected().getMoveCost() > game.getTurn().getEnergy()){
				mode = "";
			}
		}
		
		updateBattlefield();
		updatePlayerStats();
		updateButtonState();
		
	}
	
	public void directAction(ActionEvent event){
		
		if (game.getSelected() != null){
			if (game.getTurn().getEnergy() >= game.getSelected().getAttackCost()){
				if (game.attackDirectGUI()){
					// End Game
					winnerDescription.setVisible(true);
					winnerName.setText(game.getTurn().getTitle(true) + game.getTurn().getName().substring(0, 1).toUpperCase() + game.getTurn().getName().substring(1));
					winnerName.setVisible(true);
					
					//Highscore
					String currentHighscore = game.readHighscore();
					System.out.println(currentHighscore);
					int turns = -1;
					if (currentHighscore != ""){
						try {
							
							turns = Integer.parseInt(currentHighscore.substring(currentHighscore.indexOf("|")+1));
						} catch (NumberFormatException e){
							System.out.println("Failed to load correctly formatted highscores.txt");
						}
					}
					if (turns > (game.getTurn().getMaxEnergy()-100)/10 || turns == -1){
						game.writeHighscore(game.getTurn().getTitle(true) + game.getTurn().getName().substring(0, 1).toUpperCase() + game.getTurn().getName().substring(1) + "|" + Integer.toString((game.getTurn().getMaxEnergy()-100)/10));
						highscore.setText(game.getTurn().getTitle(true) + game.getTurn().getName().substring(0, 1).toUpperCase() + game.getTurn().getName().substring(1) + " has the least turns with " + Integer.toString((game.getTurn().getMaxEnergy()-100)/10) + " turns.");
					}else{
						highscore.setText(currentHighscore.substring(0,currentHighscore.indexOf("|")) + " has the least turns with " + currentHighscore.substring(currentHighscore.indexOf("|")+1) + " turns.");
					}
					highscore.setVisible(true);
					
					//Disable Game
					spawnKnight.setDisable(true);
					spawnRogue.setDisable(true);
					spawnMage.setDisable(true);
					attackSelected.setDisable(true);
					moveSelected.setDisable(true);
					endTurn.setDisable(true);
					selectedName.setVisible(false);
					selectedAttackCost.setVisible(false);
					selectedHealth.setVisible(false);
					selectedAttack.setVisible(false);
					selectedMoveCost.setVisible(false);
					mode = "";
					game.getSelected().setSelected(false);
					updatePlayerStats();
					updateBattlefield();
				}else{
					// Continue Game
					mode = "";
					updateBattlefield();
					updatePlayerStats();
					updateButtonState();
				}
			}
		}
		
	}
	
	public void endTurn(){
		game.endTurn();
		if (game.getSelected() != null){
			game.getSelected().setSelected(false);
			game.trackSelected(null,null);
		}
		mode = "";
		updateBattlefield();
		updatePlayerStats();
		updateButtonState();
	}
	
}
