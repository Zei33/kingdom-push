package net.zeiworld.kingdom_push.core;

import java.util.ArrayList;

public class Rogue extends Unit {

	public Rogue(int team, String type, Integer column, Integer row) {
		super(team, type, column, row);
		this.symbol = (team == 1 ? "♢" : "♦");
		this.hp = 60;
		this.attack = 50;
		this.moveCost = 25;
		this.attackCost = 25;
	}

	@Override
	public ArrayList<ArrayList<Integer>> getPossibleActions(String type){
		ArrayList<ArrayList<Integer>> actions = new ArrayList<ArrayList<Integer>>();
		
		if (type.equals("move")){
		
			//Rogues can move equally in all directions regardless of affiliation.
			actions.add( createAction(+1, -1) );
			actions.add( createAction(+1, 0) );
			actions.add( createAction(+2, 0) );
			actions.add( createAction(+1, +1) );
			
			actions.add( createAction(-1, -1) );
			actions.add( createAction(-1, 0) );
			actions.add( createAction(-2, 0) );
			actions.add( createAction(-1, +1) );
			
			actions.add( createAction(0, -1) );
			actions.add( createAction(0, -2) );
			actions.add( createAction(0, +1) );
			actions.add( createAction(0, +2) );
			
			actions.add( createAction(-2, -2) );
			actions.add( createAction(+2, +2) );
			
			actions.add( createAction(-2, +2) );
			actions.add( createAction(+2, -2) );
		
		}else if (type.equals("attack")){
			
			actions.add( createAction(0, -1) );
			actions.add( createAction(-1, 0) );
			actions.add( createAction(+1, 0) );
			actions.add( createAction(0, +1) );
			
		}
		
		return actions;
	}
	
	
}
