package net.zeiworld.kingdom_push.core;

import java.util.Scanner;

public class TextInterface {
	
	public static void main(String[] args){
		
		// Initialise variables.
		KingdomPush game = new KingdomPush();
		TextInterface ui = new TextInterface();
		Scanner in = new Scanner(System.in);
		String input = "";
		String command[] = null;
		
		System.out.println("--                       Kingdom Push                       --");
		System.out.println("-                Strength, Cunning and Wisdom                -");
		System.out.println("\nTo start a new match, use the 'new' command.");
		
		// Start command loop.
		ui.Command(game, in, input, command);
		
	}
	
	public void Command(KingdomPush game, Scanner in, String input, String[] command){
		
		// Receive a command.
		System.out.print( ( game.matchStarted == true ? game.getPlayer(game.getTurn().getID()).getTitle(true) + game.getTurn().getName() + " " : "" ) + "> " );
		input = in.nextLine().toString();
		
		// Split command into an array.
		command = input.toLowerCase().split(" ");
		
		// Check against commands.
		if (command.length > 0){
			
			// Check the new command.
			if (command[0].equals("new")){
				
				// Check for correct number of arguments.
				if (command.length == 3){
					
					if (game.matchStarted){
					
						System.out.println("Match already in progress.");
					
					}else{
					
						// Begin New Match (We capitalise the first letter of each name because they're lower case now).
						game.StartMatch(command[1].substring(0, 1).toUpperCase() + command[1].substring(1), command[2].substring(0, 1).toUpperCase() + command[2].substring(1));
					
					}
					
				}else{
					
					// Throw error if incorrect command usage.
					CommandError("new", "<Player 1 Name> <Player 2 Name>", "No Aliases");
					
				}
				
			}else if (command[0].equals("show")){
			
				if (game.matchStarted){
					
					if (command.length == 2){
						
						if (command[1].equals("stats")){
							
							game.showStats();
							
						}
						
						if (command[1].equals("map")){
							
							game.drawBattlefield(false);
							
						}
						
						if (command[1].equals("unit") || command[1].equals("selected")){
							
							game.showSelectedStats();
							
						}
						
						if (command[1].equals("attack")){
							
							game.drawBattlefield(true);
							
						}
						
					}else{
						
						CommandError("show", "<Stats|Map|Unit>", "No Aliases");
						
					}
					
				}else{
					
					System.out.println("You need to start a match before using that command.");
					
				}
			
			}else if (command[0].equals("create") || command[0].equals("spawn") || command[0].equals("add")){
				
				if (game.matchStarted){
				
					if (command.length == 3){
						
						if (command[1].equals("knight") || command[1].equals("mage") || command[1].equals("rogue")){
							
							// Ensure that the row the player gives is actually a number and on the battlefield.
							Integer row = -1;
							try{
								row = Integer.parseInt(command[2]);
							}catch(NumberFormatException e){
								row = -1;
							}
							
							if (row >= 0 && row <= game.getBattlefieldRows()-1){
								
								// Place the unit on the battlefield and draw the map.
								if ( game.createUnit(command[1], row) ){
									game.drawBattlefield(false);
									game.showStats();
								}
								
							}else{
								
								System.out.println("Row not within battlefield range.");
								
							}
							
						}else{
							
							CommandError("create", "<Knight|Mage|Rogue> <Row>", "spawn, add");
							
						}
						
					}else{
						
						CommandError("create", "<Knight|Mage|Rogue> <Row>", "spawn, add");
						
					}
				
				}else{
					
					System.out.println("You need to start a match before using that command.");
					
				}
				
			}else if (command[0].equals("select")){
				
				if (game.matchStarted){
					
					if (command.length == 3){
						
						Integer row = -1;
						try{
							row = Integer.parseInt(command[2]);
						}catch(NumberFormatException e){
							row = -1;
						}
						
						if (row >= 0 && row <= game.getBattlefieldRows()-1){
							
							if (game.selectUnit(command[1], row)){
								
							}else{
								
								CommandError("select", "<Column> <Row> (There must be a unit in this position)", "No Aliases");
							}
							
							
						}else{
							
							CommandError("select", "<Column> <Row>", "No Aliases");
							
						}
						
					}else{
						
						CommandError("select", "<Column> <Row>", "No Aliases");
						
					}
				
				}else{
					
					System.out.println("You need to start a match before using that command.");
				
				}
				
			}else if (command[0].equals("move")){
				
				if (game.matchStarted){
					
					if (command.length == 3){
						
						Integer row = -1;
						try{
							row = Integer.parseInt(command[2]);
						}catch(NumberFormatException e){
							row = -1;
						}
						
						if (row >= 0 && row <= game.getBattlefieldRows()-1){
							
							game.moveUnit(command[1], row);
							
						}else{
							
							CommandError("move", "<Column> <Row>", "No Aliases");
							
						}
						
					}else{
						
						CommandError("move", "<Column> <Row>", "No Aliases");
						
					}
					
				}else{
					
					System.out.println("You need to start a match before using that command.");
					
				}
				
			}else if (command[0].equals("attack")){
				
				if (game.matchStarted){
					
					if (command.length == 3){
						
						Integer row = -1;
						try{
							row = Integer.parseInt(command[2]);
						}catch(NumberFormatException e){
							row = -1;
						}
						
						if (row >= 0 && row <= game.getBattlefieldRows()-1){
							
							game.attackUnit(command[1], row);
							
						}else{
							
							CommandError("attack", "<Column> <Row>", "No Aliases");
							
						}
						
					}else{
						
						CommandError("attack", "<Column> <Row>", "No Aliases");
						
					}
					
				}else{
					
					System.out.println("You need to start a match before using that command.");
					
				}
				
			}else if (command[0].equals("end")){
					
				if (game.matchStarted){
			
					game.endTurn();
				
				}
				
			}else{
				
				System.out.println("Unknown command, type 'help' for help.");
				
			}
			
		}
		
		input = "";
		
		// If game is still running, continue loop.
		if ( game.enabled == true && input == ""){
			
			Command(game, in, input, command);
		
		}else{
			
			System.out.println("\n\nThank you for playing!\nCreated by Matthew Scott");
			
		}
		
	}
	
	public void CommandError(String commandName, String arguments, String aliases){
		System.out.println("\nCommand Usage: " + commandName + " " + arguments + "\nAliases: " + aliases);
	}
	
}
